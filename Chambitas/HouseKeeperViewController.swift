//
//  HouseKeeperViewController.swift
//  Chambitas
//
//  Created by Ana Mazín on 4/18/17.
//  Copyright © 2017 Ana Mazín. All rights reserved.
//

import UIKit

class HouseKeeperViewController: UIViewController {
    
    @IBOutlet var collectionView : UICollectionView!
    
    let manager = HouseKeeperDataManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.fetch()
    }
    
}

extension HouseKeeperViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return manager.numberOfItems()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "housekeeperCell", for: indexPath) as!HouseKeeperCell
        return cell
    }

}

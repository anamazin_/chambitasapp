//
//  WorkerAnnotation.swift
//  Chambitas
//
//  Created by Ana Mazín on 4/20/17.
//  Copyright © 2017 Ana Mazín. All rights reserved.
//

import UIKit

class WorkerAnnotation: NSObject {
    
    var name: String?
    var works: [String] = []
    var imageURL:String?
    var data:[String:AnyObject]?
    
    init(dict:[String:AnyObject]) {
        if let name = dict["name"] as? String { self.name = name }
        if let works = dict["works"] as? [String] { self.works = works }
        if let image = dict["image_url"] as? String { self.imageURL = image }
        
        data = dict
    }
    
    var workerItem:WorkerItem {
        guard let workerData = data else { return WorkerItem() }
        return WorkerItem(dict: workerData)
    }

}

//
//  HouseKeeperDataManager.swift
//  Chambitas
//
//  Created by Ana Mazín on 4/18/17.
//  Copyright © 2017 Ana Mazín. All rights reserved.
//

import Foundation

class HouseKeeperDataManager {
    
    fileprivate var items:[HouseKeeperItem] = []
    
    func fetch() {
        for data in loadData() {
            items.append(HouseKeeperItem(dict: data))
        }
    }
    
    func numberOfItems() ->Int {
        return items.count
    }
    
    func explore(at index:IndexPath) -> HouseKeeperItem {
        return items[index.item]
    }
    
    fileprivate func loadData() -> [[String: AnyObject]] {
        guard let path = Bundle.main.path(forResource: "HouseKeeperData", ofType: "plist"),
            let items = NSArray(contentsOfFile: path) else {
                return [[:]]
        }
        
        return items as! [[String : AnyObject]]
    }
    
}

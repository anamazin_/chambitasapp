//
//  HouseKeeperItem.swift
//  Chambitas
//
//  Created by Ana Mazín on 4/18/17.
//  Copyright © 2017 Ana Mazín. All rights reserved.
//

import Foundation

struct HouseKeeperItem {
    var name:String?
    var image:String?

}

extension HouseKeeperItem {
    init(dict:[String:AnyObject]) {
        self.name = dict["name"] as? String
        self.image = dict["image"] as? String
    }
    
}

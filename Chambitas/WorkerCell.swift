//
//  WorkerCell.swift
//  Chambitas
//
//  Created by Ana Mazín on 4/20/17.
//  Copyright © 2017 Ana Mazín. All rights reserved.
//

import UIKit

class WorkerCell: UICollectionViewCell {
    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var lblCity:UILabel!
    @IBOutlet var lblWork:UILabel!
    
}

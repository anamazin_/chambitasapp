//
//  HouseKeeperCell.swift
//  Chambitas
//
//  Created by Ana Mazín on 4/18/17.
//  Copyright © 2017 Ana Mazín. All rights reserved.
//

import UIKit

class HouseKeeperCell: UICollectionViewCell {
    @IBOutlet var lblName:UILabel!
    @IBOutlet var imgHouseKeeper:UIImageView!
    
}

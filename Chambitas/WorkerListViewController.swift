//
//  WorkerListViewController.swift
//  Chambitas
//
//  Created by Ana Mazín on 4/18/17.
//  Copyright © 2017 Ana Mazín. All rights reserved.
//

import UIKit

class WorkerListViewController: UIViewController {
    
    @IBOutlet var collectionView:UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension WorkerListViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "workerListCell", for: indexPath)
        return cell
    }
}

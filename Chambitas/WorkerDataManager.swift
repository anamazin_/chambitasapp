//
//  WorkerDataManager.swift
//  Chambitas
//
//  Created by Ana Mazín on 4/20/17.
//  Copyright © 2017 Ana Mazín. All rights reserved.
//

import Foundation

class WorkerDataManager {
    
    private var items:[WorkerItem] = []
    
    func numberOfItems() -> Int {
        return items.count
    }
    
    func workerItem (at index:IndexPath) -> WorkerItem {
        return items[index.item]
    }
}

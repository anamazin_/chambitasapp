//
//  WorkerItem.swift
//  Chambitas
//
//  Created by Ana Mazín on 4/20/17.
//  Copyright © 2017 Ana Mazín. All rights reserved.
//

import Foundation

struct WorkerItem {
    var name:String?
    var city:String?
    var works:[String] = []
    
    var work: String? {
        if works.isEmpty { return " " }
        else if works.count == 1 { return works.first }
        else { return works.joined(separator: ", ")}
    }
}

extension WorkerItem {
    init(dict:[String:AnyObject]) {
        name = dict["name"] as? String
        city = dict["city"] as? String
        if let works = dict["works"] as? [AnyObject] {
            for data in works {
                if let work = data["work"] as? String {
                    self.works.append(work)
                }
            }
        }
    }
}
